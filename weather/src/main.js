import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios'
import Axios from 'axios'

import App from './App.vue'
import Main from'./components/Main.vue'
import WeatherInfo from './components/WeatherInfo.vue'
Vue.config.productionTip = false

const routes = [
  { path: '/', component: Main },
  { path: '/weather-info/:cityName', component: WeatherInfo, props: true },
]

const router = new VueRouter({
  routes
})

Vue.use(VueRouter)
Vue.use(VueAxios, Axios)

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
