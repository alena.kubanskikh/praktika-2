const routes = require('./routes.js')
const cors = require('cors')
const bodyParser = require('body-parser')
const express = require('express')

app = express()
app.use(cors())
app.use(bodyParser.json());
port = process.env.PORT || 3000
mongoose = require('mongoose')
Product = require('./models/ProductModel')
Order = require('./models/OrderModel')

mongoose.connect('mongodb://localhost/magazin_db')
const db = mongoose.connection;
routes(app)
app.listen(port)