var mongoose = require('mongoose');

var orderSchema = mongoose.Schema({
    totalPrice: Number,
    itemsCount: Number,
    items: Array
});

module.exports = mongoose.model('Orders', orderSchema);