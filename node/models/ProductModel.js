var mongoose = require('mongoose');

var productSchema = mongoose.Schema({
    name: String,
    description: String,
    price: Number
});

module.exports = mongoose.model('Products', productSchema);