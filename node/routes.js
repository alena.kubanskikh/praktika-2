//const { db } = require('./models/OrderModel')
Product = require('./models/ProductModel')
Order = require('./models/OrderModel')

module.exports = function (app) {
    app.get("/products", (req, res) => {
        Product.find((err, products) => {
            if (err) res.send(err);
            res.json(products);
        })
    })

    app.get("/orders", (req, res) => {
        Order.find((err, orders) => {
            if (err) res.send(err);
            res.json(orders);
        })
    })

    app.post("/orders", (req, res) => {
        console.log(req.body)
        let o = new Order(req.body)
        o.save();
        Order.find((err, orders) => {
            if (err) res.send(err);
            res.json(orders);
        })
    })
}